=====
bline
=====

* Description: Offloads data from Baler 14's.

* Usage: bline

* Free software: GNU General Public License v3 (GPLv3)


Features
--------

* A command line interface program that enables offloading data from
  Quanterra Baler 14's, and provides a bit of QC to ensure all of the
  offloaded files have been received and are not badly corrupted.
* BaleAddr-like functionality to assign IP address to balers will be
  added in the future.
