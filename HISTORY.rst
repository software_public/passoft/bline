=======
History
=======

2019.064 (2019-03-05)
---------------------
* Converted for Python 2 or 3.
* Added -vl command that lists the files that have not been fully
  offloaded from the baler. -v just shows how many there are.
* Does not yet contain BaleAddr replacement code.

2019.259 (2019-09-16)
---------------------
* New version (really BLINE2) that assigns the IP address to the baler,
  and erases data and Q330 association from the baler, thus removing
  the need for BaleAddr.
* The TagID of the baler to communicate with is still required on the
  command line, but the IP address of the baler is no longer required for
  every command on the command line.
* The Ethernet device name may be required when assigning the IP address
  to the baler (-b command) if BLINE says so (generally Python 2).
* These non-regular Python modules may need to be installed to get all
  of the new commands (-b, -X, -s) to work:
     psutil - all OSs and Python versions
     pexpect - Linux
     subprocess32 - all OSs when using Python 2
  Python 3 is recommended, since support for 2 is going away soon.
  pip/pip3 may be used to install the required modules.

2019.269 (2019-09-26)
---------------------
* Caught a different program having trouble checking for updates. Put
  in some code as a possible fix.
* Collected the imports for all of the extra required modules into one
  place so all of the warnings come out at the same time. Use
     bline.py 5555 -b
  to get a list of the warnings. 5555 may be any number for this test.
* Fixed up the long version of the Help a bit.

2019.289 (2019-10-16)
---------------------
* Added the -A command that will combine all of the offloaded files
  into a .ALL file.
* Added the -G command that will combine all of the files for a
  channel into one file. The file names are close to the miniseed
  file names produced by sdrsplit.
* Changed the command line switch for a couple commands.

2019.297 (2019-10-24)
---------------------
* Enhanced the -G command and added the -GD command for concatenating
  the offloaded channel files.

2020.056 (2020-02-25)
---------------------
* Never knew the data file names could have the *__.<chan> underscores
  replaced with characters, so the program was not detecting high
  sample rate files correctly.

2020.201 (2020-07-20)
------------------
* Updated to work with Python 3
* Added a unit test to test bline import
* Updated list of platform specific dependencies to be installed when
  pip installing bline (see setup.py)
* Installed and tested bline against Python3.[6,7,8] using tox
* Formatted Python code to conform to the PEP8 style guide
* Created conda packages for bline that can run on Python3.[6,7,8]
* Updated .gitlab-ci.yml to run a linter and unit tests for Python3.[6,7,8]
  in GitLab CI pipeline
