#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `bline` package."""

import unittest
import sys

from unittest.mock import patch


class TestBline(unittest.TestCase):
    """Tests for `bline` package."""

    def test_import(self):
        """Test bline import"""
        with patch.object(sys, 'argv', ['bline']):
            with self.assertRaises(SystemExit):
                try:
                    import bline.bline as bp
                    self.assertTrue(bp.PROG_NAME, 'BLINE')
                except ImportError:
                    self.fail('Failed to import bline!')
